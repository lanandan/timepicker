package com.ikgr.timepicker;

import java.util.Calendar;




import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Intent;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

public class CustomizeActivity extends Activity {

	EditText edt_time;
	 // Variable for storing current date and time
    private int mYear, mMonth, mDay, mHour, mMinute;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_custom);
		// For Current Time
		
		edt_time=(EditText)findViewById(R.id.edt_dialog);
		//edt_time.setEnabled(false);
		edt_time.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				timer();
	
			}
			});
	                 
		

	}

	public void timer()
	{
		final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog tpd = new TimePickerDialog(CustomizeActivity.this, new TimePickerDialog.OnTimeSetListener() {
			
			@Override
			public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
				// TODO Auto-generated method stub
			    edt_time.setText(hourOfDay + ":" + minute);
            }
        }, mHour, mMinute, false);
        tpd.show();
	}

	public void setClick(View v) {
		Intent timer = new Intent();
		switch (v.getId()) {
		case R.id.preview_custom:
			timer.setClass(CustomizeActivity.this, StartEndActivity.class);
			startActivity(timer);
			finish();
			overridePendingTransition(R.anim.right_in, R.anim.left_out);
			break;
		}
	}
}
