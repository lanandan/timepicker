package com.ikgr.timepicker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AnalogActivity extends Activity{

	
	Button btn_anlog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_analog);
		btn_anlog=(Button)findViewById(R.id.btn_preview_analog);
		btn_anlog.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent timer=new Intent();
				timer.setClass(AnalogActivity.this, HomeActivity.class);
				startActivity(timer);
				finish();
				overridePendingTransition(R.anim.left_out, R.anim.right_in);
			}
		});	
		
	}

}
