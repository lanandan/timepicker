package com.ikgr.timepicker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class HomeActivity extends Activity {

	Button btn_normal, btn_custom, btn_digital, btn_analog;
	View v;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mainpage);

		btn_normal = (Button) findViewById(R.id.btn_normal);
		btn_digital = (Button) findViewById(R.id.btn_digital);
		btn_analog = (Button) findViewById(R.id.btn_analog);

	}

	public void onClick(View v) {
		Intent timer = new Intent();
		switch (v.getId()) {
		case R.id.btn_normal:
			timer.setClass(HomeActivity.this, MainActivity.class);
			break;
		case R.id.btn_digital:
			timer.setClass(HomeActivity.this, DigitalActivity.class);
			break;
		case R.id.btn_analog:
			timer.setClass(HomeActivity.this, AnalogActivity.class);
			break;
		}
		startActivity(timer);
		finish();
		overridePendingTransition(R.anim.right_in, R.anim.left_out);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.animation_enter,
				R.anim.animation_leave);

	}

}
