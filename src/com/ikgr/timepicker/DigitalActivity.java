package com.ikgr.timepicker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class DigitalActivity extends Activity{
	View v;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_digital);
	}
	
	public void digitalclick(View v){
		Intent timer = new Intent();
		switch (v.getId()) {
		case R.id.btn_dig_next:
			timer.setClass(DigitalActivity.this, CounterActivity.class);
			startActivity(timer);
			overridePendingTransition(R.anim.right_in, R.anim.left_out);
			break;
			
		case R.id.btn_dig_preview:
			timer.setClass(DigitalActivity.this, HomeActivity.class);
			startActivity(timer);
			overridePendingTransition(R.anim.left_out, R.anim.right_in);
			break;
		}
		
	}

}
