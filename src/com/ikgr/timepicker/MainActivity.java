package com.ikgr.timepicker;

import java.util.Calendar;

import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;

public class MainActivity extends Activity {

	TextView txt_display;
	TimePicker timepicker1;
	private int hour, minute;
	View v;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// For Current Time
		setCurrentTimeOnView();
	}

	public void setCurrentTimeOnView() {

		txt_display = (TextView) findViewById(R.id.display_Time);
		timepicker1 = (TimePicker) findViewById(R.id.tmp_first);

		final Calendar c = Calendar.getInstance();
		hour = c.get(Calendar.HOUR_OF_DAY);
		minute = c.get(Calendar.MINUTE);

		// set current time into textview
		txt_display.setText(new StringBuilder().append(pad(hour)).append(":")
				.append(pad(minute)));

		// set current time into timepicker
		timepicker1.setCurrentHour(hour);
		timepicker1.setCurrentMinute(minute);

	}

	
	private static String pad(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}
	
	
	public void setClick(View v) {
		Intent timer = new Intent();
		switch (v.getId()) {
		case R.id.btn_next:
			timer.setClass(MainActivity.this, SetChangingTime.class);
			startActivity(timer);
			finish();
			overridePendingTransition(R.anim.right_in, R.anim.left_out);
			
			break;
		case R.id.btn_preview:
			timer.setClass(MainActivity.this, HomeActivity.class);
			startActivity(timer);
			finish();
			overridePendingTransition(R.anim.left_out, R.anim.right_in);
			break;
		
		}
		
		
	}
	
	 @Override
		public void onBackPressed() {
			super.onBackPressed();
			overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
			
		}
}
