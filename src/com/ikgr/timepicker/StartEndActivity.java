package com.ikgr.timepicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class StartEndActivity extends Activity {

	CheckBox optSingleShot;
	Button btnStart, btnEnd;
	TextView textCounter;

	Timer timer;
	MyTimerTask myTimerTask;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_timer_startandend);
		// For Current Time
		
		  optSingleShot = (CheckBox)findViewById(R.id.singleshot);
		  btnStart = (Button)findViewById(R.id.start);
		  btnEnd = (Button)findViewById(R.id.end);
		  textCounter = (TextView)findViewById(R.id.counter);
		  
		  btnStart.setOnClickListener(new OnClickListener(){

		   @Override
		   public void onClick(View arg0) {

		    if(timer != null){
		     timer.cancel();
		    }
		    timer = new Timer();
		    myTimerTask = new MyTimerTask();
		    
		    if(optSingleShot.isChecked()){
		     //singleshot delay 1000 ms
		     timer.schedule(myTimerTask, 1000);
		    }else{
		     //delay 1000ms, repeat in 5000ms
		     timer.schedule(myTimerTask, 1000, 5000);
		    }
		   }
		   });
		  
		  btnEnd.setOnClickListener(new OnClickListener(){

			   @Override
			   public void onClick(View v) {
			    if (timer!=null){
			     timer.cancel();
			     timer = null;
			    }
			   }
			  });

	}
	
	
	public void  startEnd(View v){
		Intent timer = new Intent();
		switch (v.getId()) {
		case R.id.Preview:
			timer.setClass(StartEndActivity.this, SetChangingTime.class);
			startActivity(timer);
			finish();
			overridePendingTransition(R.anim.left_out, R.anim.right_in);
			break;
		case R.id.next_end:
			timer.setClass(StartEndActivity.this, CustomizeActivity.class);
			startActivity(timer);
			finish();
			overridePendingTransition(R.anim.right_in, R.anim.left_out);
			
			break;
		}
		
		
	}

	class MyTimerTask extends TimerTask {

		@Override
		public void run() {
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd:MMMM:yyyy HH:mm:ss a");
			final String strDate = simpleDateFormat.format(calendar.getTime());

			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					textCounter.setText(strDate);
				}
			});
		}

	}

}
