package com.ikgr.timepicker;

import java.util.Calendar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;

public class SetChangingTime extends Activity {
	TextView txt_display;
	TimePicker timepicker1;
	private int hour, minute;
	private String format = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_changingtime);
		// For Current Time
		timepicker1=(TimePicker)findViewById(R.id.tmp_picker);
		txt_display = (TextView) findViewById(R.id.display_Times);

	}
	
	
	public void onclick(View v) {
		Intent timer = new Intent();
		switch (v.getId()) {
		case R.id.btn_ch_next:
			timer.setClass(SetChangingTime.this,StartEndActivity.class);
			startActivity(timer);
			finish();
			overridePendingTransition(R.anim.right_in, R.anim.left_out);
			
			break;
		case R.id.btn_ch_preview:
			timer.setClass(SetChangingTime.this, MainActivity.class);
			startActivity(timer);
			finish();
			overridePendingTransition(R.anim.left_out, R.anim.right_in);
			break;
		
		}
		
		
	}
	

	public void setTime(View view) {
		hour = timepicker1.getCurrentHour();
		minute = timepicker1.getCurrentMinute();
		showTime(hour, minute);
	}

	public void showTime(int hour, int min) {
		if (hour == 0) {
			hour += 12;
			format = "AM";
	
		} else if (hour == 12) {
			format = "PM";
		} else if (hour > 12) {
			hour -= 12;
			format = "PM";
		} else {
			format = "AM";
		}
		txt_display.setText(new StringBuilder().append(hour).append(" : ")
				.append(min).append(" ").append(format));


	}

	private static String pad(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}
	
	 @Override
		public void onBackPressed() {
			super.onBackPressed();
			overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);
			
		}
}

