package com.ikgr.timepicker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CounterActivity extends Activity {

	private Button startButton;
	private Button pauseButton,clearButton;
	private TextView timerValue;
	private long startTime = 0L;
	private Handler customHandler = new Handler();
	long timeInMilliseconds = 0L;
	long timeSwapBuff = 0L;
	long updatedTime = 0L;
	View v;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		setContentView(R.layout.activity_digitalcustom);
		// For Current Time
		timerValue = (TextView) findViewById(R.id.timerValue);
		startButton=(Button)findViewById(R.id.startButton);
		startButton.setOnClickListener(new View.OnClickListener() {

			public void onClick(View view) {

				startTime = SystemClock.uptimeMillis();

				customHandler.postDelayed(updateTimerThread, 0);
			}
		});
		pauseButton = (Button) findViewById(R.id.pauseButton);
		pauseButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				timeSwapBuff += timeInMilliseconds;
				customHandler.removeCallbacks(updateTimerThread);

			}
		});
		

	}

	private Runnable updateTimerThread = new Runnable() {
		public void run() {
			timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
			updatedTime = timeSwapBuff + timeInMilliseconds;
			int secs = (int) (updatedTime / 1000);
			int mins = secs / 60;
			secs = secs % 60;
			int milliseconds = (int) (updatedTime % 1000);
			timerValue.setText("" + mins + ":" + String.format("%02d", secs)
					+ ":" + String.format("%03d", milliseconds));
			customHandler.postDelayed(this, 0);
		}

	};

	public void onclick(View v) {
		Intent timer = new Intent();
		switch (v.getId()) {
		case R.id.btn_cus_preview:
			timer.setClass(CounterActivity.this, DigitalActivity.class);
			break;
		
		}
		startActivity(timer);
		finish();
		overridePendingTransition(R.anim.right_in, R.anim.left_out);
	
	}
}
